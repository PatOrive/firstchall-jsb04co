"use strict";

// 1. CALCULADORA

let number1 = parseInt(prompt("Dime un numero"));
console.log(number1);

let number2 = parseInt(prompt("Dime otro numero"));
console.log(number2);

let sign = prompt("Dime operador: +, -, *, /, **");
console.log(sign);

function calculator(n1, n2) {
  if (sign === "+") {
    return n1 + n2;
  } else if (sign === "-") {
    return n1 - n2;
  } else if (sign === "*") {
    return n1 * n2;
  } else if (sign === "/") {
    return n1 / n2;
  } else if (sign === "**") {
    return n1 ** n2;
  }
}

let resultado = console.log(calculator(number1, number2));

// 2. DADO ELECTRONICO

let totalScore = 0;

function die() {
  let roll = Math.round(Math.random() * (6 - 1) + 1);
  console.log("Tirada = ", roll);
  totalScore = totalScore + roll;
  console.log("Puntuacion total = ", totalScore);
  if (totalScore < 50) {
    die();
  } else {
    console.log("Fin de la partida");
  }
}

die();

// 3. REGISTRO ACADÉMICO

class Person {
  name;
  age;
  gender;
  constructor(name, age, gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
  }
}

let PersonOne = new Person();
console.log(Object.keys(PersonOne));

class Student extends Person {
  subject;
  group;
  constructor(name, age, gender, subject, group) {
    super(name, age, gender);
    this.subject = subject;
    this.group = group;
  }

  static createTotalStudentsList(names, ages, gender, subject, group) {
    return names.map((name, index) => {
      return new Student(
        name,
        ages[index],
        gender[index],
        subject[index],
        group[index]
      );
    });
  }
}

const names = [
  "Ana",
  "Beatriz",
  "Camilo",
  "Diego",
  "Emilio",
  "Fatima",
  "Gustavo"
];
const ages = [19, 21, 18, 20, 31, 18, 23];
const gender = ["female", "female", "male", "male", "male", "female", "male"];
const subject = [
  "English",
  "Chinese",
  "French",
  "French",
  "Chinese",
  "English",
  "English"
];
const group = [
  "Level 3",
  "Level 1",
  "Level 2",
  "Level 2",
  "Level 1",
  "Level 3",
  "Level 3"
];

const students = Student.createTotalStudentsList(
  names,
  ages,
  gender,
  subject,
  group
);
console.log(students);

class Teacher extends Person {
  subject;
  group;
  studentsList = [];
  constructor(name, age, gender, subject, group) {
    super(name, age, gender);
    this.subject = subject;
    this.group = group;
  }

  // He intentado asignar los alumnos de cada profesor, pero no lo he conseguido:

  assign(students) {
    for (let i = 0; i < students.length; i++) {
      for (let j = 0; j < teachers.lenght; j++) {
        if (
          student[i].subject === teacher[j].subject &&
          student[i].group === teacher[j].group
        ) {
          teacher[j].studentsList.push(student[i]);
        }
      }
    }
  }
}

const namesT = ["Helen", "Gilles", "Lao"];
const agesT = [35, 42, 51];
const genderT = ["female", "male", "male"];
const subjectT = ["English", "French", "Chinese"];
const groupT = ["Level 3", "Level 2", "Level 1"];

const teachers = namesT.map((name, index) => {
  return new Teacher(
    name,
    agesT[index],
    genderT[index],
    subjectT[index],
    groupT[index]
  );
});

console.log(teachers);

const theachersWithStudents = teachers.assign(students);
console.log(teachersWithStudents);
